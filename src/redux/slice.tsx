import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import Record from '../Record';

const initialState: {
  records: Record[],
  filteredRecords: Record[],
  page: number,
  search: string,
  recordsOnPage: number
} = {
  records: [],
  filteredRecords: [],
  page: 1,
  search: '',
  recordsOnPage: 10
};

const getFilteredRecords = (records: Record[], searchLowerCase: string) => {
  return records.filter(record =>
    searchLowerCase === '' ||
    record.id.toString().includes(searchLowerCase) ||
    record.title.toLowerCase().includes(searchLowerCase) ||
    record.body.toLowerCase().includes(searchLowerCase)
  );
}

export const appSlice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    setRecords: (state, action: PayloadAction<Record[]>) => {
      state.records = action.payload;
      state.filteredRecords = getFilteredRecords(state.records, state.search.toLowerCase());
    },
    setPage: (state, action: PayloadAction<number>) => {
      state.page = action.payload;
    },
    setSearch: (state, action: PayloadAction<string>) => {
      state.search = action.payload;
      state.filteredRecords = getFilteredRecords(state.records, state.search.toLowerCase());
    }
  },
});

export const {setRecords, setPage, setSearch} = appSlice.actions;

export default appSlice.reducer;
