import './App.scss';
import React, {useEffect} from 'react';
import {useSearchParams} from 'react-router-dom';
import {useAppDispatch, useAppSelector} from './redux/hooks';
import axios from 'axios';
import {setPage, setRecords} from './redux/slice';
import Search from './elements/Search';
import Table from './elements/Table';
import Pagination from './elements/Pagination';

const App = () => {
  const dispatch = useAppDispatch();
  const filteredRecords = useAppSelector((state) => state.app.filteredRecords);
  const [searchParams] = useSearchParams();

  useEffect(() => {
    (async () => {
      try {
        const result = await axios.get('https://jsonplaceholder.typicode.com/posts');
        if (result.data) {
          dispatch(setRecords(result.data));
        }
      } catch {
      }
    })();
    dispatch(setPage(parseInt(searchParams.get('page') ?? '1')));
  }, []);

  return (
    <div className='app'>
      <Search/>
      <Table/>
      {
        filteredRecords.length ?
          <Pagination/> :
          <p className='not-found'>Записи не найдены</p>
      }
    </div>
  );
}

export default App;
