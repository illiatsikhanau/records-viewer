import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import {Provider} from 'react-redux';
import {store} from './redux/store';
import {BrowserRouter, Navigate, Route, Routes} from 'react-router-dom';

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <Provider store={store}>
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<App/>}/>
        <Route path='*' element={<Navigate to='/?page=1'/>}/>
      </Routes>
    </BrowserRouter>
  </Provider>
);
