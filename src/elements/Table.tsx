import './Table.scss';
import React, {useEffect, useState} from 'react';
import {useAppDispatch, useAppSelector} from '../redux/hooks';
import {setRecords} from '../redux/slice';

enum Field {ID, TITLE, BODY}

enum Direction {UP, DOWN}

interface Sort {
  field: Field,
  direction: Direction
}

const defaultSort: Sort = {field: Field.ID, direction: Direction.UP};

const Table = () => {
  const dispatch = useAppDispatch();
  const records = useAppSelector((state) => state.app.records);
  const filteredRecords = useAppSelector((state) => state.app.filteredRecords);
  const page = useAppSelector((state) => state.app.page);
  const recordsOnPage = useAppSelector((state) => state.app.recordsOnPage);
  const [pageRecords, setPageRecords] = useState(filteredRecords);
  const [sort, setSort] = useState(defaultSort);

  useEffect(() => {
    const sortedRecords = [...records];

    sortedRecords.sort((r1, r2) => {
      if (sort.field === Field.ID) {
        return sort.direction === Direction.UP ?
          r1.id - r2.id :
          r2.id - r1.id;
      } else {
        const fieldName = sort.field === Field.TITLE ? 'title' : 'body';
        return sort.direction === Direction.UP ?
          r1[fieldName].localeCompare(r2[fieldName]) :
          r2[fieldName].localeCompare(r1[fieldName]);
      }
    });

    dispatch(setRecords(sortedRecords));
  }, [sort]);

  useEffect(() => {
    setPageRecords(filteredRecords.slice((page - 1) * recordsOnPage, page * recordsOnPage));
  }, [filteredRecords, page, recordsOnPage]);

  const setRecordsSort = (field: Field) => {
    if (field === sort.field) {
      setSort({...sort, direction: sort.direction === Direction.UP ? Direction.DOWN : Direction.UP});
    } else {
      setSort({field: field, direction: Direction.UP});
    }
  }

  return (
    <table className='table'>
      <thead className='table-head'>
      <tr>
        <th>
          <div
            className={`table-head-element ${sort.field === Field.ID ? 'selected' : ''}`}
            onClick={() => setRecordsSort(Field.ID)}
          >
            ID
            <img
              className={`arrow ${sort.field === Field.ID && sort.direction === Direction.UP ? 'up' : ''}`}
              src='/assets/arrow-down.svg' alt=''
            />
          </div>
        </th>
        <th>
          <div
            className={`table-head-element ${sort.field === Field.TITLE ? 'selected' : ''}`}
            onClick={() => setRecordsSort(Field.TITLE)}
          >
            Заголовок
            <img
              className={`arrow ${sort.field === Field.TITLE && sort.direction === Direction.UP ? 'up' : ''}`}
              src='/assets/arrow-down.svg' alt=''
            />
          </div>
        </th>
        <th>
          <div
            className={`table-head-element ${sort.field === Field.BODY ? 'selected' : ''}`}
            onClick={() => setRecordsSort(Field.BODY)}
          >
            Описание
            <img
              className={`arrow ${sort.field === Field.BODY && sort.direction === Direction.UP ? 'up' : ''}`}
              src='/assets/arrow-down.svg' alt=''
            />
          </div>
        </th>
      </tr>
      </thead>
      <tbody>
      {pageRecords.map(record =>
        <tr key={record.id}>
          <td className='table-body-element center'>{record.id}</td>
          <td className='table-body-element'>{record.title}</td>
          <td className='table-body-element'>{record.body}</td>
        </tr>
      )}
      </tbody>
    </table>
  );
}

export default Table;
