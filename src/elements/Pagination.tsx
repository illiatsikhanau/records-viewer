import './Pagination.scss';
import React, {useEffect, useState} from 'react';
import {useAppDispatch, useAppSelector} from '../redux/hooks';
import {setPage} from '../redux/slice';
import {useSearchParams} from 'react-router-dom';

const Pagination = () => {
  const dispatch = useAppDispatch();
  const filteredRecords = useAppSelector((state) => state.app.filteredRecords);
  const page = useAppSelector((state) => state.app.page);
  const recordsOnPage = useAppSelector((state) => state.app.recordsOnPage);

  const [_, setSearchParams] = useSearchParams();
  const [maxPage, setMaxPage] = useState(1);
  const [pages, setPages] = useState([1]);

  useEffect(() => {
    setMaxPage(Math.max(1, Math.ceil(filteredRecords.length / recordsOnPage)));
  }, [filteredRecords, recordsOnPage]);

  useEffect(() => {
    if (maxPage <= 5) {
      setPages(Array.from({length: maxPage}, (_, i) => i + 1));
    } else {
      if (page <= 3) {
        setPages([1, 2, 3, 4, 5]);
      } else if (page <= maxPage - 2) {
        setPages([page - 2, page - 1, page, page + 1, page + 2]);
      } else {
        setPages([maxPage - 4, maxPage - 3, maxPage - 2, maxPage - 1, maxPage]);
      }
    }
  }, [maxPage, page]);

  const prevPage = () => {
    setCurrentPage(Math.max(page - 1, 1));
  }

  const nextPage = () => {
    setCurrentPage(Math.min(page + 1, 10));
  }

  const setCurrentPage = (page: number) => {
    dispatch(setPage(page));
    setSearchParams({'page': page.toString()});
  }

  return (
    <ul className='pagination'>
      <li className='previous' onClick={() => prevPage()}>Назад</li>
      <li className='pages'>
        <div
          className={`page ${page === 1 ? 'active' : ''}`}
          onClick={() => setCurrentPage(1)}
        >
          1
        </div>
        <div>...</div>
        {pages.map(p =>
          <div
            key={p}
            className={`page ${page === p ? 'active' : ''}`}
            onClick={() => setCurrentPage(p)}
          >
            {p}
          </div>
        )}
        <div>...</div>
        <div
          className={`page ${page === maxPage ? 'active' : ''}`}
          onClick={() => setCurrentPage(maxPage)}
        >
          {maxPage}
        </div>
      </li>
      <li className='next' onClick={() => nextPage()}>Далее</li>
    </ul>
  );
}

export default Pagination;
