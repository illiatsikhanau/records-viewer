import './Search.scss';
import React, {ChangeEvent} from 'react';
import {useAppDispatch, useAppSelector} from '../redux/hooks';
import {setPage, setSearch} from '../redux/slice';
import {useSearchParams} from 'react-router-dom';

const Search = () => {
  const dispatch = useAppDispatch();
  const search = useAppSelector((state) => state.app.search);
  const [_, setSearchParams] = useSearchParams();

  const changeSearch = (e: ChangeEvent<HTMLInputElement>) => {
    dispatch(setPage(1));
    setSearchParams({'page': '1'});
    dispatch(setSearch(e.target.value));
  };

  return (
    <div className='search'>
      <input
        className='search-input'
        type='text'
        placeholder='Поиск'
        value={search}
        onChange={changeSearch}
      />
      <img className='search-icon' src='/assets/search.svg' alt=''/>
    </div>
  );
}

export default Search;
